import org.jetbrains.kotlin.config.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.22"
    kotlin("plugin.serialization") version "1.7.22"
    application
}

group = "com.github.doomsdayrs.lib"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("com.squareup:kotlinpoet:1.12.0")
    implementation("io.github.pdvrieze.xmlutil:core:0.84.3")
    implementation("io.github.pdvrieze.xmlutil:serialization:0.84.3")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JvmTarget.JVM_17.description
}

application {
    mainClass.set("MainKt")
}