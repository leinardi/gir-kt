import builder.Core

fun main(args: Array<String>) {

		val core = Core(
			"/usr/share/gir-1.0/Gtk-4.0.gir",
			"build/gir-kt/"
		)
		core.execute()

}