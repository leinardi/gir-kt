package model

/*
 * 23 / 03 / 2022
 */
const val XMLNS = "http://www.gtk.org/introspection/core/1.0"
const val XMLNS_C = "http://www.gtk.org/introspection/c/1.0"
const val XMLNS_GLIB = "http://www.gtk.org/introspection/glib/1.0"
const val XMLNS_XML = "http://www.w3.org/XML/1998/namespace"
