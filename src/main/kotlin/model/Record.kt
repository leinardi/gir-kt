package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("record", XMLNS, "")
data class Record(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	val disguised: Boolean = false,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String?,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String?,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String?,

	val foreign: Boolean = false,

	@XmlSerialName("is-gtype-struct-for", XMLNS_GLIB, "glib")
	val glibIsGTypeStructFor: String?,


	val fields: List<Field> = emptyList(),

	val functions: List<GIRFunction> = emptyList(),

	val unions: List<Union> = emptyList(),

	val method: List<GIRMethod> = emptyList(),

	val constructors: List<GIRConstructor> = emptyList(),

	val sourcePosition: SourcePosition?,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : InfoAttrs, InfoElements