package model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("namespace", XMLNS, "")
data class NameSpace(
	val docsection: DocSection?, // TODO Unknown

	val name: String?,

	val version: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cIdentifierPrefixes: String?,

	@XmlSerialName("symbol-prefixes", XMLNS_C, "c")
	val cSymbolPrefixes: String?,

	@XmlSerialName("prefix", XMLNS_C, "c")
	val cPrefix: String?,

	@SerialName("shared-library")
	val sharedLibrary: String?,

	val aliases: List<Alias> = emptyList(),

	val classes: List<GIRClass> = emptyList(),

	val interfaces: List<Interface> = emptyList(),

	val records: List<Record> = emptyList(),

	val enums: List<Enumeration> = emptyList(),

	val functions: List<GIRFunction> = emptyList(),

	val unions: List<Union> = emptyList(),

	val bitfields: List<Bitfield> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val constants: List<Constant> = emptyList(),

	val annotations: List<GIRAnnotation> = emptyList(),

	val boxes: List<Boxed> = emptyList(),

	val functionMacros: List<FunctionMacro> = emptyList(),
)