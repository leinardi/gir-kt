package model

/**
 * 25 / 03 / 2022
 */
interface DocElements {

	val `doc-version`: DocVersion?

	val `doc-stability`: DocStability?

	val `doc`: Doc?

	val `doc-deprecated`: DocDeprecated?

	val `source-position`: SourcePosition?

}