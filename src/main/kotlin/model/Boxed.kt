package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

/**
 * 23 / 03 / 2022
 */
@Serializable
@XmlSerialName("boxed", XMLNS_GLIB, "glib")
data class Boxed(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	@XmlSerialName("name", XMLNS_GLIB, "glib")
	val name: String,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String,

	val functions: List<GIRFunction> = emptyList(),
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>
) : InfoAttrs,InfoElements