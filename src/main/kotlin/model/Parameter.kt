package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("parameter", XMLNS, "")
data class Parameter(
	val name: String,

	@XmlSerialName("transfer-ownership", "", "")
	val transferOwnership: String?,

	val type: Type?,

	val nullable: Boolean = false,

	@XmlSerialName("allow-none", "", "")
	val allowNone: Boolean = false,

	val closure: Int?,

	val scope: String?,
	val destroy: Int?,

	val array: GIRArray?,

	val varargs: VarArgs?,

	val direction: String?,

	@XmlSerialName("caller-allocates", "", "")
	val callerAllocates: Boolean = false,

	val optional: Boolean = false,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
):DocElements