package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("include", XMLNS_C, "c")
data class CInclude(
	val name: String,
)