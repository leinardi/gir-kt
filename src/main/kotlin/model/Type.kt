package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("type", XMLNS, "")
data class Type(
	val name: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	val introspectable: Boolean = false,

	@XmlSerialName("type", XMLNS, "")
	val type: Type? = null,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	val array: GIRArray?

) : DocElements