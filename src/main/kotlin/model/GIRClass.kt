package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("class", XMLNS, "")
data class GIRClass(
	val name: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String,

	val parent: String?,

	@XmlSerialName("type-struct", XMLNS_GLIB, "glib")
	val glibTypeStruct: String?,

	@XmlSerialName("ref-func", XMLNS_GLIB, "glib")
	val glibRefFunc: String?,

	@XmlSerialName("unref-func", XMLNS_GLIB, "glib")
	val glibUnRefFunc: String?,

	@XmlSerialName("set-value-func", XMLNS_GLIB, "glib")
	val glibSetValueFunc: String?,

	@XmlSerialName("get-value-func", XMLNS_GLIB, "glib")
	val glibGetValueFunc: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String,

	val abstract: Boolean = false,

	@XmlSerialName("fundamental", XMLNS_GLIB, "glib")
	val glibFundamental: Boolean = false,

	val final: Boolean = false,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,

	val implements: List<Implements> = emptyList(),

	val constructor: List<GIRConstructor> = emptyList(),

	val methods: List<GIRMethod>,

	val functions: List<GIRFunction> = emptyList(),

	val virtualMethods: List<VirtualMethod> = emptyList(),

	val fields: List<Field> = emptyList(),

	val properties: List<Property>,

	val signals: List<Signal>,

	val unions: List<Union> = emptyList(),

	val constants: List<Constant> = emptyList(),

	val records: List<Record> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val sourcePosition: SourcePosition?,

	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	) : InfoAttrs, InfoElements