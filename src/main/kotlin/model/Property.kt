package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("property", XMLNS, "")
data class Property(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	val writable: Boolean = false,

	val readable: Boolean = false,

	val construct: Boolean = false,

	val `construct-only`: Boolean = false,

	val setter: String?,

	val getter: String?,

	val `transfer-ownership`: String,

	val attribute: GIRAnnotation?,

	val array: GIRArray?,

	val type: Type?,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : InfoAttrs, InfoElements