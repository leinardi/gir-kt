package model

/**
 * 25 / 03 / 2022
 */
interface InfoElements : DocElements {
	val annotations: List<model.GIRAnnotation>
}