package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.*

/**
 * 23 / 02 / 2022
 */

@Serializable
@XmlSerialName("repository", XMLNS, "")
data class GIRRepository(
	val version: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cIdentifierPrefixes: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cSymbolPrefixes: String?,

	val includes: List<Include>,

	val cInclude: List<CInclude>,

	val pack: List<Package>,

	val namespace: List<NameSpace>
)


