package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("function", XMLNS, "")
data class GIRFunction(
	override val name: String,
	@XmlSerialName("identifier", XMLNS_C, "c")
	override val cIdentifier: String?,
	override val `shadowed-by`: String?,
	override val shadows: String?,
	override val throws: Boolean = false,
	override val `moved-to`: String?,
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val parameters: Parameters?,

	val returnValue: ReturnValue?,

	val sourcePosition: SourcePosition?,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
) : CallableAttrs, DocElements