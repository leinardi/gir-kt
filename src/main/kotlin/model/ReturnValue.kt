package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("return-value", XMLNS, "")
data class ReturnValue(
	val introspectable: Boolean = false,

	val nullable: Boolean = false,

	val closure: Int?,

	val scope: Scope?,

	val destroy: Int?,

	val skip: Boolean = false,

	@Deprecated("Replaced by nullable & optional")
	val `allow-none`: Boolean = false,

	val `transfer-ownership`: String?,

	val type: Type?,

	val array: GIRArray?,

	val attribute: GIRAnnotation?,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?
) : DocElements