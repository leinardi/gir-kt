package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("source-position", XMLNS, "")
data class SourcePosition(
	val filename: String,

	val line: Int,

	val column: Int?
)