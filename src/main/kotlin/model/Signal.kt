package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("signal", XMLNS_GLIB, "glib")
data class Signal(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	val detailed: Boolean = false,

	@XmlElement(false)
	val `when`: When?,

	val action: Boolean = false,

	val `no-hooks`: Boolean = false,
	val `no-recurse`: Boolean = false,

	val emitter: String?,

	val parameters: Parameters?,

	val returnValue: ReturnValue,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
	override val doc: Doc?,
) : InfoAttrs,InfoElements