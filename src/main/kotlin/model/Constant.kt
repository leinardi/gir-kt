package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("constant", XMLNS, "")
data class Constant(
	val name: String,

	val value: String,
	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String,

	val type: Type,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val stability: String?, override val version: String?
) : InfoAttrs,InfoElements