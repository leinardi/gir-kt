package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("alias", XMLNS, "")
data class Alias(

	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,
	override val `doc-version`: DocVersion?,
	override val annotations: List<GIRAnnotation>,

	val name: String,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String,

	val type: Type,


	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?
) : InfoAttrs, InfoElements