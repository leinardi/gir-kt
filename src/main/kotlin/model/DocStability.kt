package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import nl.adaptivity.xmlutil.serialization.XmlValue

/**
 * 25 / 03 / 2022
 */

@Serializable
@XmlSerialName("doc-stability", XMLNS, "")
data class DocStability(
	@XmlSerialName("space", XMLNS_XML, "xml")
	val space: String?,
	@XmlSerialName("whitespace", XMLNS_XML, "xml")
	val whitespace: String?,

	@XmlValue(true)
	val text: String
)
