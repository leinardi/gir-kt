package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("field", XMLNS, "")
data class Field(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	val writable: Boolean = false,

	val readable: Boolean = false,

	val private: Boolean = false,

	val bits: Int?,

	val type: Type?,

	val callback: GIRCallback?,

	val array: GIRArray? = null,
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : InfoAttrs, InfoElements