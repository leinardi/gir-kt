package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import nl.adaptivity.xmlutil.serialization.XmlValue

@Serializable
@XmlSerialName("doc-deprecated", XMLNS, "")
data class DocDeprecated(
	@XmlSerialName("space", XMLNS_XML, "xml")
	val space: String?,

	@XmlSerialName("whitespace", XMLNS_XML, "xml")
	val whitespace: String?,

	@XmlValue(true)
	val text: String
)