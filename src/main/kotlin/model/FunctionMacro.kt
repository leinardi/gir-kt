package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("function-macro", XMLNS, "")
data class FunctionMacro(
    val name: String,
    @XmlSerialName("identifier", XMLNS_C, "c")
    val cIdentifier: String,
    val parameters: Parameters?,

    override val `doc-version`: DocVersion?,
    override val `doc-stability`: DocStability?,
    override val doc: Doc?,
    override val `doc-deprecated`: DocDeprecated?,
    override val `source-position`: SourcePosition?,

    override val introspectable: Boolean?,
    override val deprecated: Boolean?,
    override val `deprecated-version`: String?,
    override val version: String?,
    override val stability: String?,

    ) : DocElements, InfoAttrs