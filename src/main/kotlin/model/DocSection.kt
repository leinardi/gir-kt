package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

/**
 * 25 / 03 / 2022
 *
 * TODO Investigate
 */
@Serializable
@XmlSerialName("docsection", XMLNS, "")
data class DocSection(
	val doc: Doc,
	val name:String?
)