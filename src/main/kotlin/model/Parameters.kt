package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("parameters", XMLNS, "")
data class Parameters(
	val parameters: List<Parameter> = emptyList(),

	val instanceParameter: List<InstanceParameter> = emptyList()
)