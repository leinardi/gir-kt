package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("array", XMLNS, "")
data class GIRArray(
    @XmlSerialName("type", XMLNS_C, "c")
    val cType: String?,
    val type: Type?,
    val name: String?,
    val length: Int?,
    @XmlSerialName("zero-terminated", "", "")
    val zeroTerminated: Boolean = true,

    @XmlSerialName("fixed-size", "", "")
    val fixedSize: Boolean = false,

    val array: GIRArray?
)