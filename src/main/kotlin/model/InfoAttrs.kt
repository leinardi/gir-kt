package model

/**
 * 23 / 03 / 2022
 */
interface InfoAttrs {

	val introspectable: Boolean?

	val deprecated: Boolean?

	val `deprecated-version`: String?

	val version: String?

	val stability: String?
}