package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("interface", XMLNS, "")
data class Interface(

	val name: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	@XmlSerialName("type-struct", XMLNS_GLIB, "glib")
	val glibTypeStruct: String? = null,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,

	val prerequisites: List<Prerequisite> = emptyList(),

	val implements: List<Implements> = emptyList(),

	val functions: List<GIRFunction> = emptyList(),

	val constructors: List<GIRConstructor> = emptyList(),

	val methods: List<GIRMethod> = emptyList(),

	val virtualMethods: List<VirtualMethod> = emptyList(),

	val fields: List<Field> = emptyList(),

	val properties: List<Property> = emptyList(),

	val signals: List<Signal> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val constants: List<Constant> = emptyList(),

	val sourcePosition: SourcePosition?,

	override val introspectable: Boolean?,

	override val deprecated: Boolean?,

	override val `deprecated-version`: String?,

	override val version: String?,

	override val stability: String?,

) : InfoAttrs, InfoElements