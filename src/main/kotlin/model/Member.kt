package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("member", XMLNS, "")
data class Member(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	val value: Long,

	@XmlSerialName("identifier", XMLNS_C, "c")
	val cIdentifier: String,

	@XmlSerialName("nick", XMLNS_GLIB, "glib")
	val glibNick: String?,

	@XmlSerialName("name", XMLNS_GLIB, "glib")
	val glibName: String?,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : InfoAttrs, InfoElements