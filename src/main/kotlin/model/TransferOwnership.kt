package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

/**
 * 24 / 03 / 2022
 */
@Serializable
@XmlSerialName("transfer-ownership", XMLNS, "")
enum class TransferOwnership {
	none,
	container,
	full
}