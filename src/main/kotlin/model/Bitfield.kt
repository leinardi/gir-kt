package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("bitfield", XMLNS, "")
data class Bitfield(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String?,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String?,

	val members: List<Member> = emptyList(),
	val functions: List<GIRFunction> = emptyList(),
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>, override val doc: Doc?
) : InfoAttrs, InfoElements