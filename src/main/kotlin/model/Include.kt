package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("include", XMLNS, "")
data class Include(
	val name: String,
	val version: String?
)