package model

import kotlinx.serialization.Serializable

/**
 * 25 / 03 / 2022
 */
@Serializable
enum class Scope {
	notified,
	async,
	call,
	forever
}