package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("prerequisite", XMLNS, "")
data class Prerequisite(
	val name: String
)