package model

import nl.adaptivity.xmlutil.serialization.XmlSerialName


/**
 * 24 / 03 / 2022
 */
interface CallableAttrs : InfoAttrs {
	val name: String

	@XmlSerialName("identifier", XMLNS_C, "c")
	val cIdentifier: String?

	val `shadowed-by`: String?

	val shadows: String?

	val throws: Boolean

	val `moved-to`: String?
}