package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("attribute", XMLNS, "")
data class GIRAnnotation(
	val name: String,
	val value: String
)