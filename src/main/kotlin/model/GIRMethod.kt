package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("method", XMLNS, "")
data class GIRMethod(
	override val name: String,
	@XmlSerialName("identifier", XMLNS_C, "c")
	override val cIdentifier: String?,
	override val `shadowed-by`: String?,
	override val shadows: String?,
	override val throws: Boolean = false,
	override val `moved-to`: String?,
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	@XmlSerialName("set-property", XMLNS_GLIB, "glib")
	val glibSetProperty: String?,

	@XmlSerialName("get-property", XMLNS_GLIB, "glib")
	val glibGetProperty: String?,

	@XmlSerialName("shadowed-by", "", "")
	val shadowedBy: String?,

	val attribute: GIRAnnotation?,

	@XmlSerialName("deprecated-version", XMLNS, "")
	val deprecatedVersion: String? = null,

	val parameters: Parameters,

	val returnValue: ReturnValue,

	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val doc: Doc?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : CallableAttrs, InfoElements