package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.QName
import nl.adaptivity.xmlutil.serialization.XmlCData
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import nl.adaptivity.xmlutil.serialization.XmlValue

@Serializable
@XmlSerialName("doc", XMLNS, "")
data class Doc(
	@XmlSerialName("space", XMLNS_XML, "xml")
	val space: String?,

	@XmlSerialName("whitespace", XMLNS_XML, "xml")
	val whitespace: String?,

	val filename: String,

	val line: Int,

	val column: String?,

	@XmlValue(true)
	val text: String
)