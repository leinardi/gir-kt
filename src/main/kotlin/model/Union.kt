package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

/**
 * 25 / 03 / 2022
 */

@Serializable
@XmlSerialName("union", XMLNS, "")
data class Union(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String?,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String?,
	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String?,


	val fields: List<Field> = emptyList(),
	val constructors: List<GIRConstructor> = emptyList(),
	val methods: List<GIRMethod> = emptyList(),
	val functions: List<GIRFunction> = emptyList(),
	val records: List<Record> = emptyList(),
	override val `doc-version`: DocVersion?,
	override val `doc-stability`: DocStability?,
	override val `doc-deprecated`: DocDeprecated?,
	override val `source-position`: SourcePosition?,
	override val annotations: List<GIRAnnotation>,
	override val doc: Doc?
) : InfoAttrs, InfoElements
