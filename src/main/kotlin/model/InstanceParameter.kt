package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("instance-parameter", XMLNS, "")
data class InstanceParameter(
    val name: String,
    @XmlSerialName("transfer-ownership", "", "")
    val transferOwnership: String,
    val nullable: Boolean = false,
    @XmlSerialName("allow-none", "", "")
    val allowNone: Boolean = false,

    val direction: String?,
    val `caller-allocates`: Boolean = false,

    val type: Type,
    override val `doc-version`: DocVersion?,
    override val `doc-stability`: DocStability?,
    override val doc: Doc?,
    override val `doc-deprecated`: DocDeprecated?,
    override val `source-position`: SourcePosition?
) : DocElements