package model

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("implements", XMLNS, "")
data class Implements(
	val name: String
)