package builder

import logging.info
import model.GIRRepository
import model.Include
import nl.adaptivity.xmlutil.serialization.XML
import java.io.File

/**
 * 25 / 03 / 2022
 */
class Core(
	targetFilePath: String, outputDirectoryPath: String
) {
	val xml = XML

	/**
	 * All the parsed repositories, distinct
	 */
	private val repos = HashMap<String, GIRRepository>()

	/**
	 * How many times each repository is depended on
	 */
	private val counts = HashMap<String, Int>()

	private val mainFile = File(targetFilePath)
	private val outputDirectory = File(outputDirectoryPath)

	private fun loopInclude(includes: List<Include>, depth: Int = 0) {
		includes.forEach { include: Include ->
			val tab = StringBuilder("").apply {
				if (depth > 0)
					for (i in 0 until depth)
						append("\t")
			}.toString()

			println("${tab}Parsing GIR of ${include.name}")


			val file = File(mainFile.parentFile, "${include.name}-${include.version}.gir")

			if (!file.exists()) {
				println("${tab}WARNING: `${include.name}-${include.version}` Does not have a GIR file, skipping.")
				return@forEach
			}

			val repo = xml.decodeFromString<GIRRepository>(file.readText())
			println("${tab}Finished parsing GIR of `${include.name}`")

			// Save the work
			repos[include.name] = repo

			// Increment the count
			counts[include.name] = counts.getOrDefault(include.name, 0) + 1


			println("${tab}Parsing includes of `${include.name}`")
			loopInclude(repo.includes, depth + 1)
			println("${tab}Finished Parsing includes of `${include.name}`")
		}
	}


	fun execute() {
		println("Starting")

		println("Parsing main repo")

		val mainRepo = xml.decodeFromString<GIRRepository>(mainFile.readText())
		repos["HEAD"] = mainRepo
		counts["HEAD"] = 0

		println("Parsed main repo")

		println("Parsing includes of main repo")
		loopInclude(mainRepo.includes)
		println("Finished Parsing includes of main repo")

		println("Generating kotlin from repos from bottom to top")
		counts.entries.sortedByDescending { it.value }.map { it.key }.forEach { repoName ->
			/**
			 * Directory of this repo
			 */
			val repoDir = File(outputDirectory, repoName)
			repoDir.mkdirs()
			val repo = repos[repoName]!!
			info("Building $repoName")
			val builder = RepositoryBuilder(repo, repoDir)
			builder.build()
		}
		println("Finished")
	}
}