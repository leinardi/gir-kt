package builder

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.TypeSpec
import logging.info
import logging.warning
import model.GIRFunction

/**
 * 09 / 11 / 2022
 *
 * Builds a standalone function
 */
class FunctionBuilder(
	private val kotlinPackage: String,
	private val girFunction: GIRFunction
) {

	fun build(builder: TypeSpec.Builder) {
		info("Adding function `${girFunction.name}`")
		// Ignore vararg functions
		if (girFunction.parameters?.parameters?.any { it.name == "..." } == true) {
			warning("Ignoring vararg function: ${girFunction.cIdentifier}")
			return
		}

		builder.addFunction(
			FunSpec.builder(girFunction.name.snakeToCamelCase()).apply {
				if (girFunction.doc != null) addKdoc("%L\n", girFunction.doc.text)

				girFunction.parameters?.parameters?.forEach {
					addParameter(
						ParameterSpec.builder(
							it.name.snakeToCamelCase(),
							it.resolveTypeName(kotlinPackage)
						)
							.build()
					)
				}

				girFunction.returnValue
			}.build()
		)
	}
}