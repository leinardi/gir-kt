package builder

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.UNIT
import logging.info
import model.GIRMethod

/**
 * 09 / 11 / 2022
 */
class MethodBuilder(
	private val kotlinPackage: String,
	private val pointerName: String,
	private val girMethod: GIRMethod,
	private val addImportFunction: AddImportFunction
) {

	fun build(builder: TypeSpec.Builder) {
		builder.addFunction(
			FunSpec.builder(girMethod.name.snakeToCamelCase()).apply {
				info("Adding method ${girMethod.cIdentifier} ")

				if (girMethod.doc != null) addKdoc("%L\n", girMethod.doc.text)

				girMethod.parameters.parameters.forEach {
					addParameter(
						ParameterSpec.builder(
							it.name.snakeToCamelCase(),
							it.resolveTypeName(kotlinPackage)
						)
							.build()
					)
				}

				var returns = false

				girMethod.returnValue.let { returnType ->
					val className = returnType.resolveTypeName(kotlinPackage)
					if (className != UNIT) {
						returns(className.copy(nullable = girMethod.returnValue.nullable))
						returns = true
					}
				}
				girMethod.returnValue.array?.let { returnArray ->
					returns = false
					// TODO handle returnArray for girMethod
				}

				val statementBuilder = StringBuilder("")

				if (returns)
					statementBuilder.append("return ")

				statementBuilder.append(girMethod.cIdentifier)

				statementBuilder.append("(")

				statementBuilder.append(pointerName)

				girMethod.parameters.parameters.forEach {
					statementBuilder.append(", ")
						.append(generateKotlinToCParameter(it, addImportFunction))
				}

				statementBuilder.append(")")

				addStatement(
					"%L",
					statementBuilder.toString()
				)
			}.build()
		)
	}
}