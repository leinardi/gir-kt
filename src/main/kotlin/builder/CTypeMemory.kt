package builder

/**
 * 13 / 12 / 2022
 *
 * Contains the memory of what CTypes exist, useful for when a return type
 *  only tells us the CType and not what the CType is.
 */
object CTypeMemory {
	enum class ClassType {
		ENUM,
		DIRECT,
		WRAPABLE,
	}

	data class ClassDescriptor(
		val name: String,
		val type: ClassType,
		val import: String,
		val cType: String
	)

	private val memory = ArrayList<ClassDescriptor>()

	fun find(search: String): ClassDescriptor? =
		memory.find { it.cType == search } ?: memory.find { it.name == search }

	fun add(classDescriptor: ClassDescriptor) {
		memory.add(classDescriptor)
	}

	private fun addGlib(name: String) {
		add(
			ClassDescriptor(
				name,
				ClassType.DIRECT,
				"glib",
				name
			)
		)
	}

	init {
		addGlib("gboolean")
		addGlib("gint")
		addGlib("gpointer")
		addGlib("gfloat")
		addGlib("gunichar")
	}
}