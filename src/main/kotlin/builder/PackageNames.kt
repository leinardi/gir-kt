package builder

/**
 * 29 / 12 / 2022
 */
fun resolveKotlinPackageName(givenPackage: String): String =
	when (givenPackage) {
		"cairo-gobject" -> "org.cairographics.cairo"
		"gtk4" -> "org.gtk"
		"gdk-pixbuf-2.0" -> "org.gtk.gdk.pixbuf"
		"gio-2.0" -> "org.gtk.gio"
		"glib-2.0" -> "org.gtk.glib"
		"gmodule-2.0" -> "org.gtk.gmodule"
		"gobject-2.0" -> "org.gtk.gobject"
		"graphene-gobject-1.0" -> "io.github.ebassi.graphene"
		"harfbuzz-gobject" -> "io.github.harfbuzz.gobject"
		"pango" -> "org.gnome.pango"
		else -> givenPackage
	}

fun resolveCPackageName(givenPackage: String):String =
	when (givenPackage) {
		"cairo-gobject" -> "cairo"
		"gtk4" -> "gtk"
		"gdk-pixbuf-2.0" -> "pixbuf"
		"gio-2.0" -> "gio"
		"glib-2.0" -> "glib"
		"gmodule-2.0" -> "gmodule"
		"gobject-2.0" -> "gobject"
		"graphene-gobject-1.0" -> "graphene"
		"harfbuzz-gobject" -> "harfbuzz"
		"pango" -> givenPackage
		else -> givenPackage
	}