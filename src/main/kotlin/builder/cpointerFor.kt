package builder

import com.squareup.kotlinpoet.TypeVariableName

fun cpointerFor(name: String) =
	TypeVariableName(
		"CPointer<$name>",
	)