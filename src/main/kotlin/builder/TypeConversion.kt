package builder

import logging.errorOut
import model.GIRArray
import model.Parameter
import model.ReturnValue
import model.Type

/**
 * Generate a C to Kotlin conversion
 */
fun generateCToKotlin(
	source: Any,
	array: GIRArray?,
	type: Type?,
	isNullable: Boolean = false,
	addImport: AddImportFunction
): String = buildString {

	append(if (isNullable) "?" else "!!")

	var foundMapping = false

	if (array != null) {
		when (array.type?.name) {
			"utf8" -> {
				addImport("org.gtk.glib", "toArray")
				append(".toArray()")
				foundMapping = true
			}

			else -> {
			}
		}
	}
	if (type != null) {
		when (type.name) {
			// Object, while initialized early on, is needed by some classes before it.
			"Object" -> {
				addImport("org.gtk.gobject", "Object.wrap")
				append(".wrap()")
				foundMapping = true
			}

			"gboolean" -> {
				addImport("org.gtk.glib", "bool")
				append(".bool")
				foundMapping = true
			}

			"utf8" -> {
				addImport("kotlinx.cinterop", "toKString")
				append(".toKString()")
				foundMapping = true
			}

			// < -- we can ignore, it maps to kotlin automatically
			"guint" -> {
				foundMapping = true
			}

			"gint" -> {
				foundMapping = true
			}

			"double" -> {
				foundMapping = true
			}

			"gdouble" -> {
				foundMapping = true
			}

			"guint16" -> {
				foundMapping = true
			}

			"gunichar" -> {
				// TODO Verify
				foundMapping = true
			}

			"gfloat" -> {
				foundMapping = true
			}

			"gsize" -> {
				foundMapping = true
			}

			"gint64" -> {
				foundMapping = true
			}

			// -- >

			"none" -> {
				// TODO Investigate this
			}

			else -> {
				val cType = type.cType
				if (cType != null) {
					val classDescriptor = CTypeMemory.find(cType)
					if (classDescriptor != null) {
						when (classDescriptor.type) {
							CTypeMemory.ClassType.ENUM -> {
								appendLine(".let { gtk -> ")
									.appendLine(classDescriptor.name + ".valueOf(gtk)")
									.append("}")
							}

							CTypeMemory.ClassType.DIRECT -> {
								// We can ignore translation, we do not need
								// to duplicate ints
							}

							CTypeMemory.ClassType.WRAPABLE -> {
								append(".wrap()")
							}
						}
						foundMapping = true
					} else {
						when (cType) {
							"const char*" -> {
								append(".toKString()")
								foundMapping = true
							}
						}
					}
				}
			}
		}
	}
	if (!foundMapping) {
		logging.errorOut("Cannot find mapping for $source")
	}
}

/**
 * Generate a c return parser.
 *
 * This works for any function with a normal return (no pointer setting, only a normal return).
 *
 * Expects cReturn and parsers it
 */
fun generateCReturnToKotlin(
	returnValue: ReturnValue,
	addImport: AddImportFunction
): String {
	val statement = StringBuilder("cReturn")
	statement.append(
		generateCToKotlin(
			returnValue,
			returnValue.array,
			returnValue.type,
			returnValue.nullable,
			addImport
		)
	)
	return statement.toString()
}

/**
 * Parameter input parsing
 */
fun generateKotlinToCParameter(
	parameter: Parameter,
	addImport: AddImportFunction
): String {
	val statement = StringBuilder(parameter.name)

	statement.append(if (parameter.nullable) "?" else "")

	if (parameter.array != null) {
		when (parameter.array.type?.name) {
			"utf8" -> {
				addImport("org.gtk.glib", "toNullTermCStringArray")
				statement.append(".toNullTermCStringArray()")
			}
		}
	}
	if (parameter.type != null) {
		if (parameter.type.name != null) {
			when (parameter.type.name) {
				"gboolean" -> {
					addImport("org.gtk.glib", "gtk")
					statement.append(".gtk")
				}

				else -> {
					val memory = CTypeMemory.find(parameter.type.name)
					if (memory != null) {
						addImport(memory.import, memory.name)
						when (memory.type) {
							CTypeMemory.ClassType.WRAPABLE -> {
								addImport("kotlinx.cinterop", "reinterpret")
								statement.append(".pointer.reinterpret()")
							}

							CTypeMemory.ClassType.ENUM -> {
								statement.append(".value")
							}

							CTypeMemory.ClassType.DIRECT -> {
								// Leave as is
							}
						}
					} else {
						errorOut("generateKotlinToCParameter: No memory found for parameter $parameter")
					}
				}
			}
		} else {
			errorOut("generateKotlinToCParameter: parameter type name was null, $parameter")
		}
	} else {
		errorOut("generateKotlinToCParameter: parameter type was null, $parameter")
	}



	return statement.toString()
}

typealias AddImportFunction = (packageName: String, name: String) -> Unit