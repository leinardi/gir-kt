package builder

import com.squareup.kotlinpoet.*
import model.Enumeration
import model.Member
import model.NameSpace
import java.io.File

/**
 * 20 / 11 / 2022
 */
class EnumBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val nameSpace: NameSpace,
	private val enumeration: Enumeration
) {
	fun build() {
		repoDir.mkdirs()

		val lowercaseName = enumeration.name.lowercase()
		val pointerName = "${lowercaseName}Pointer"
		val enumOriginal = TypeVariableName(enumeration.cType)


		val fileSpec = FileSpec.builder(kotlinPackageName, enumeration.name).apply {
			this.indent("\t")
			addType(
				TypeSpec.enumBuilder(enumeration.name).apply {
					addImport(cPackageName, enumOriginal.name)
					CTypeMemory.add(
						CTypeMemory.ClassDescriptor(
							enumeration.name,
							CTypeMemory.ClassType.ENUM,
							kotlinPackageName,
							enumeration.cType
						)
					)
					enumeration.doc?.let {
						addKdoc("%L\n", it.text)
					}

					primaryConstructor(
						FunSpec.constructorBuilder().addParameter(
							"value", enumOriginal
						).build()
					)

					enumeration.members.forEach { member: Member ->
						addImport(cPackageName, enumOriginal.name + "." + member.cIdentifier)
						addEnumConstant(
							member.name.uppercase(),
							TypeSpec.anonymousClassBuilder()
								.addSuperclassConstructorParameter(
									member.cIdentifier
								)
								.apply {
									member.doc?.let {
										addKdoc("%L\n", it.text)
									}
								}
								.build()
						)
					}

					addProperty(
						PropertySpec.builder(pointerName, enumOriginal)
							.initializer(pointerName)
							.build()
					)

					addType(
						TypeSpec.companionObjectBuilder()
							.apply {
								addFunction(
									FunSpec.builder("valueOf")
										.addParameter(ParameterSpec("gtk", enumOriginal))
										.returns(
											ClassName(
												"",
												enumeration.name
											)
										)
										.beginControlFlow("return when(gtk)")
										.apply {
											enumeration.members.forEach { member: Member ->
												val cId = member.cIdentifier
												val name = member.name.uppercase()

												addStatement(
													"$cId -> $name"
												)
											}
										}
										.endControlFlow()
										.build()
								)
							}.build()
					)
				}.build()
			)
		}.build()

		fileSpec.writeTo(repoDir)
	}
}