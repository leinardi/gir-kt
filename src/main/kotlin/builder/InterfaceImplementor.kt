package builder

import com.squareup.kotlinpoet.*
import model.Implements

/**
 * 01 / 02 / 2023
 */
fun TypeSpec.Builder.implementInterface(
	fileSpecBuilder: FileSpec.Builder,
	parentPointerName: String,
	implements: Implements,
	kotlinPackageName: String,
) {
	val memoryType = CTypeMemory.find(implements.name)

	val className: ClassName = if (memoryType != null) {
		ClassName(
			memoryType.import,
			memoryType.name
		)
	} else {
		ClassName(
			kotlinPackageName, // Assume it is in the same package, unbuilt
			implements.name
		)
	}

	// TODO import ctype

	fileSpecBuilder.addImport(className.packageName, className.simpleName)

	addSuperinterface(className)

	if (memoryType != null) {
		addProperty(
			PropertySpec.builder(
				memoryType.name + "Pointer",
				cpointerFor(memoryType.cType),
				listOf(KModifier.OVERRIDE)
			)
				.initializer("$parentPointerName.reinterpret()")
				.build()
		)
	}
}