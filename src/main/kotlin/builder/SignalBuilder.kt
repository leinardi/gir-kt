package builder

import com.squareup.kotlinpoet.*
import logging.info
import model.Signal
import java.util.*

/**
 * 06 / 01 / 2023
 */
class SignalBuilder(
	val kotlinPackage: String,
	val fileBuilder: FileSpec.Builder,

	val parentName: String,
	val pointerType: TypeVariableName,
	val classBuilder: TypeSpec.Builder,
	val companion: TypeSpec.Builder,
	val signal: Signal
) {
	val className: String = parentName

	fun build() {
		info("Adding signal `${signal.name}`")
		fileBuilder.addImport("glib", "gpointer")
		val signalNameEmbed =
			signal.name.dashToCamelCase()
				.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

		val typeAliasName = "${className}${signalNameEmbed}Func"
		val staticFuncName = "static$typeAliasName"

		val funcName = "addOn${signalNameEmbed}Callback"

		fileBuilder.addTypeAlias(
			TypeAliasSpec.builder(
				typeAliasName,
				LambdaTypeName.get(
					receiver = ClassName(
						kotlinPackage,
						className
					),
					parameters = if (signal.parameters != null) {
						signal.parameters.parameters.map {
							ParameterSpec.unnamed(
								it.resolveTypeName(kotlinPackage)
							)
						}
					} else {
						emptyList()
					},
					returnType = signal.returnValue.resolveTypeName(kotlinPackage),
				)
			).apply {
				addKdoc("%L\n", "Function type alias for [$className.$funcName]")
			}.build()
		)


		fileBuilder.addImport(
			staticCFunctionClassName.packageName,
			staticCFunctionClassName.simpleName,
		)
		fileBuilder.addImport(
			asStableRefClassName.packageName,
			asStableRefClassName.simpleName,
		)
		companion.addProperty(
			PropertySpec.builder(
				staticFuncName,
				gcallbackClassName
			).initializer(
				CodeBlock.Builder()
					.apply {
						beginControlFlow(
							buildString {
								append(staticCFunctionClassName.simpleName)
								append("{")
								append(" self : ${pointerType.name}")
								signal.parameters?.parameters?.forEach { parameter ->
									append(", ")
									append(parameter.name.snakeToCamelCase())
									append(": ")
									append(parameter.resolveCTypeName())

									parameter.type?.cType?.let { cType ->
										CTypeMemory.find(cType)?.let {
											fileBuilder.addImport(
												packageName = it.import,
												it.name
											)
										}
									}
								}
								append(", data: gpointer")
								append(" ->")
							},
						)
						addStatement("%L", "data.asStableRef<$typeAliasName>()")
						addStatement("%L", "\t.get()")
						addStatement("%L", buildString {
							append("\t.invoke(")
							append("self.wrap()")
							signal.parameters?.parameters?.forEachIndexed { index, param ->
								append(", ")
								append(param.name.snakeToCamelCase())
								append(
									generateCToKotlin(
										param,
										param.array,
										param.type,
										param.nullable
									) { p, n ->
										fileBuilder.addImport(
											packageName = p,
											n
										)
									}
								)
							}
							append(")")
						})
						endControlFlow()
						addStatement(".reinterpret()")
					}.build()
			).apply {
				addModifiers(KModifier.PRIVATE)
				addKdoc("%L\n", "Static C function for [$className.$funcName]")
			}
				.build()
		)

		fileBuilder.addImport(
			addSignalCallbackClassName.packageName,
			addSignalCallbackClassName.simpleName
		)
		classBuilder.addFunction(
			FunSpec.builder(funcName)
				.returns(ClassName("org.gtk.gobject", "SignalManager"))
				.apply {
					if (signal.doc != null) addKdoc("%L\n", signal.doc.text)
					addKdoc("%L\n", "@see [$staticFuncName]")
					addKdoc("%L\n", "@see [$typeAliasName]")
				}
				.addParameter("callback", ClassName(kotlinPackage, typeAliasName))
				.addCode(CodeBlock.builder().apply {
					addStatement("return addSignalCallback(callback)")
				}.build())
				.build()
		)
		//
	}

	companion object {
		val gcallbackClassName = ClassName(
			"gobject",
			"GCallback"
		)


		val addSignalCallbackClassName = ClassName(
			"org.gtk.gobject",
			"addSignalCallback"
		)

		val staticCFunctionClassName = ClassName(
			"kotlinx.cinterop",
			"staticCFunction"
		)

		val asStableRefClassName = ClassName(
			"kotlinx.cinterop",
			"asStableRef"
		)
	}
}