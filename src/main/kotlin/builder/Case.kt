package builder

import java.util.*

/*
 * 09 / 11 / 2022
 */

val snakeRegex = "_[a-zA-Z]".toRegex()
val dashRegex = "-[a-zA-Z]".toRegex()

fun String.snakeToCamelCase(): String {
	return snakeRegex.replace(this) {
		it.value.replace("_", "")
			.uppercase(Locale.getDefault())
	}
}

fun String.dashToCamelCase(): String {
	return dashRegex.replace(this) {
		it.value.replace("-", "")
			.uppercase(Locale.getDefault())
	}
}