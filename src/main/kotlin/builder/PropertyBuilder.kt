package builder

import com.squareup.kotlinpoet.*
import logging.info
import model.GIRMethod
import model.Property

/**
 * 09 / 11 / 2022
 */
class PropertyBuilder(
	private val kotlinPackage: String,
	private val pointerName: String,

	private val propertyName: String,
	private val property: Property,

	private val getMethod: GIRMethod?,
	private val setMethod: GIRMethod?,
	private val addImportFunction: AddImportFunction
) {
	val propertyClass: TypeName by lazy { property.resolveTypeName(kotlinPackage) }

	fun build(builder: TypeSpec.Builder) {
		info("Adding property `$propertyName` with class `$propertyClass`")
		if (propertyName == "license-type") {
			println("mark")
		}

		builder.addProperty(
			PropertySpec.builder(propertyName.dashToCamelCase(), propertyClass)
				.apply {
					if (property.doc != null) addKdoc("%L\n", property.doc.text)

					if (getMethod != null) {
						info("Adding method ${getMethod.cIdentifier} ")
						getter(
							FunSpec.getterBuilder().apply {

								if (getMethod.doc != null)
									addKdoc(
										"%L\n",
										getMethod.doc.text
									)

								addStatement(
									"val cReturn = %L",
									"${getMethod.cIdentifier!!}($pointerName)"
								)

								addStatement(
									"return %L",
									generateCReturnToKotlin(
										getMethod.returnValue,
										addImportFunction
									)
								)
							}
								.build()
						)
					}

					mutable(setMethod != null)
					if (setMethod != null) {
						info("Adding method ${setMethod.cIdentifier} ")
						val setMethodParam = setMethod.parameters.parameters.first()
						setter(
							FunSpec.setterBuilder()
								.apply {
									if (setMethod.doc != null) addKdoc(
										"%L\n",
										setMethod.doc.text
									)

									addParameter(
										ParameterSpec.builder(
											setMethodParam.name,
											setMethodParam.resolveTypeName(kotlinPackage)
										).build(),
									)

									addStatement(
										"%L",
										(setMethod.cIdentifier ?: "null") +
												"($pointerName" + ", " +
												generateKotlinToCParameter(
													setMethodParam,
													addImportFunction
												) +
												")"
									)
								}
								.build()
						)
					}
				}
				.build()
		)
	}
}