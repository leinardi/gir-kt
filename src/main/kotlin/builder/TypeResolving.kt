package builder

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import logging.warning
import model.*

/**
 * Ensures a type name is converted to the proper kotlin version
 *
 * IE `gboolean` -> `Boolean`
 */
fun String.getKotlinClass(defaultPackage: String): TypeName {
	return when (this) {
		"gboolean" -> BOOLEAN

		"gint" -> INT
		"guint" -> U_INT
		"guint16" -> U_SHORT
		"guint32" -> U_INT

		"gsize" -> U_LONG

		"utf8" -> STRING

		"gchar" -> CHAR

		"gdouble" -> DOUBLE

		"gpointer" -> {
			TypeVariableName(
				"CPointer<out kotlinx.cinterop.CPointed>",
			)
		}

		"none" -> UNIT
		else -> {
			ClassName(
				defaultPackage,
				this.substringAfter(".")
			)
		}
	}
}

fun Property.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, type, array, defaultPackage)

fun ReturnValue.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, type, array, defaultPackage)

fun Parameter.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, type, array, defaultPackage)

fun Parameter.resolveCTypeName(): String =
	resolveCTypeName(this, type, array)

fun resolveCTypeName(parent: Any, type: Type?, array: GIRArray?): String {
	if (type != null) {
		if (type.cType != null)
			return type.cType

		if (type.name != null) {
			CTypeMemory.find(type.name)?.let { memory ->
				return memory.cType
			}
		}
	}
	warning("Failed to resolve CTypeName for $parent")
	return "TODO()"
}

fun resolveTypeName(parent: Any, type: Type?, array: GIRArray?, defaultPackage: String): TypeName {
	if (type != null) {
		if (type.name != null) {
			return type.name.getKotlinClass(defaultPackage)
		} else {
			warning("Type has no name, investigate $parent")
		}
	}
	if (array != null) {
		if (array.type != null) {
			// this is a primitive array

			if (array.type.name != null) {
				val arrayType = array.type.name.getKotlinClass(defaultPackage)
				return ARRAY.parameterizedBy(arrayType)
			} else {
				warning("Type has no name, investigate $parent")
			}
		} else {
			// this is an object array
			return UNIT
		}
	}

	throw Exception("Cannot resolve type name")
}