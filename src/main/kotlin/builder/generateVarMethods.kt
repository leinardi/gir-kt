package builder

import com.squareup.kotlinpoet.TypeSpec
import logging.warning
import model.GIRMethod
import model.Property

fun TypeSpec.Builder.generateVarMethods(
	properties: List<Property>,
	kotlinPackageName: String,
	pointerName: String,
	girMethods: List<GIRMethod>,
	addImportFunction: AddImportFunction
) {
	// Create var via their getter/setter combinations
	girMethods
		.filter { it.glibSetProperty != null || it.glibGetProperty != null }
		// group by their property names
		.groupBy { it.glibGetProperty ?: it.glibSetProperty!! }
		// only work with things that have 1 input / output
		.filter { propertyAccessors ->
			propertyAccessors.value.any { it.parameters.parameters.size <= 1 }
		}.let { propertyAccessors ->
			propertyAccessors.forEach { (propertyName, methods) ->
				val property = properties.find { it.name == propertyName }!!
				val builder = PropertyBuilder(
					kotlinPackageName,
					pointerName,
					propertyName,
					property,
					methods.find { it.glibGetProperty != null },
					methods.find { it.glibSetProperty != null },
					addImportFunction
				)

				builder.build(this)
			}
		}
}

fun TypeSpec.Builder.generateMethods(
	kotlinPackageName: String,
	pointerName: String,
	girMethods: List<GIRMethod>,
	addImportFunction: AddImportFunction
) {
	girMethods
		// only work with methods that are not singular getter/setter
		.filter { (it.glibGetProperty == null && it.glibSetProperty == null) || it.parameters.parameters.size > 1 }
		.forEach { girMethod ->
			// Ignore vararg functions, there is no way to implement them due to being compile time dependent for C
			if (girMethod.parameters.parameters.any { it.name == "..." }) {
				warning("Ignoring vararg method: ${girMethod.cIdentifier}")
				return
			}

			val builder =
				MethodBuilder(kotlinPackageName, pointerName, girMethod, addImportFunction)
			builder.build(this)
		}
}
