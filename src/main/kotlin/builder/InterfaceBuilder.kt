package builder

import com.squareup.kotlinpoet.*
import logging.info
import model.Interface
import model.NameSpace
import java.io.File

/**
 * 09 / 11 / 2022
 */
class InterfaceBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val nameSpace: NameSpace,
	private val interfaze: Interface
) {
	fun build() {
		info("Adding interface `${interfaze.name}`")
		repoDir.mkdirs()

		val lowercaseName = interfaze.name.lowercase()
		val pointerName = if (interfaze.name == "Object") {
			"pointer"
		} else {
			"${lowercaseName}Pointer"
		}
		val pointerTypeName = interfaze.cType ?: interfaze.glibTypeName
		val pointerType = cpointerFor(pointerTypeName)

		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, interfaze.name)
		fileSpecBuilder.indent("\t")
		val typeSpecBuilder = TypeSpec.interfaceBuilder(interfaze.name).apply {
			fileSpecBuilder.addImport("kotlinx.cinterop", "CPointer")
			fileSpecBuilder.addImport(cPackageName, pointerTypeName)

			CTypeMemory.add(
				CTypeMemory.ClassDescriptor(
					interfaze.name,
					CTypeMemory.ClassType.WRAPABLE,
					kotlinPackageName,
					pointerTypeName
				)
			)
			interfaze.doc?.let {
				addKdoc("%L\n", it.text)
			}

			addProperty(
				PropertySpec.builder(pointerName, pointerType)
					.build()
			)

			interfaze.implements.forEach { implements ->
				implementInterface(fileSpecBuilder, pointerName, implements, kotlinPackageName)
			}

			// Import methods
			interfaze.methods.forEach {
				fileSpecBuilder.addImport(cPackageName, it.cIdentifier!!)
			}

			generateVarMethods(
				interfaze.properties,
				kotlinPackageName,
				pointerName,
				interfaze.methods
			) { p, n ->
				fileSpecBuilder.addImport(p, n)
			}

			generateMethods(kotlinPackageName, pointerName, interfaze.methods) { p, n ->
				fileSpecBuilder.addImport(p, n)
			}

			val companionObj = TypeSpec.companionObjectBuilder()
				.apply {
					interfaze.functions.forEach {
						val builder = FunctionBuilder(kotlinPackageName, it)
						builder.build(this)
					}
					addFunction(
						FunSpec.builder("wrap")
							.receiver(
								cpointerFor(
									pointerTypeName
								).copy(nullable = true)
							)
							.returns(ClassName("", interfaze.name).copy(nullable = true))
							.addStatement("return·this?.wrap()").build()
					)
					addFunction(
						FunSpec.builder("wrap")
							.receiver(cpointerFor(pointerTypeName))
							.returns(ClassName("", interfaze.name))
							.addStatement("return ${interfaze.name}(this)")
							.build()
					)
				}

			interfaze.signals.forEach { signal ->
				SignalBuilder(
					kotlinPackageName,
					fileSpecBuilder,
					interfaze.name,
					pointerType,
					this,
					companionObj,
					signal
				).build()
			}

			addType(
				companionObj.build()
			)
		}.build()
		fileSpecBuilder.addType(typeSpecBuilder)
		fileSpecBuilder.build().writeTo(repoDir)
	}
}