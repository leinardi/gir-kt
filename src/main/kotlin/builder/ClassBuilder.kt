package builder

import com.squareup.kotlinpoet.*
import logging.info
import model.GIRClass
import model.NameSpace
import java.io.File

/**
 * 09 / 11 / 2022
 */
class ClassBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val nameSpace: NameSpace,
	private val clazz: GIRClass
) {

	fun build() {
		info("Adding class `${clazz.name}`")
		repoDir.mkdirs()

		val lowercaseName = clazz.name.lowercase()
		val pointerName = if (clazz.name == "Object") {
			"pointer"
		} else {
			"${lowercaseName}Pointer"
		}
		val pointerTypeName = clazz.cType ?: clazz.glibTypeName
		val pointerType = cpointerFor(pointerTypeName)

		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, clazz.name)
		fileSpecBuilder.indent("\t")
		val typeSpecBuilder = TypeSpec.classBuilder(clazz.name).apply {
			fileSpecBuilder.addImport("kotlinx.cinterop", "CPointer")
			fileSpecBuilder.addImport(cPackageName, pointerTypeName)

			CTypeMemory.add(
				CTypeMemory.ClassDescriptor(
					clazz.name,
					CTypeMemory.ClassType.WRAPABLE,
					kotlinPackageName,
					pointerTypeName
				)
			)
			clazz.doc?.let {
				addKdoc("%L\n", it.text)
			}

			primaryConstructor(
				FunSpec.constructorBuilder().addParameter(
					pointerName, pointerType
				).build()
			)

			if (!clazz.final) {
				addModifiers(KModifier.OPEN)
			}

			addProperty(
				PropertySpec.builder(pointerName, pointerType).initializer(pointerName)
					.build()
			)

			if (clazz.parent != null) {
				val memoryType = CTypeMemory.find(clazz.parent)
				if (memoryType != null) {
					superclass(
						ClassName(
							memoryType.import,
							memoryType.name
						)
					)
				} else {
					superclass(
						ClassName(
							kotlinPackageName, // Assume it is in the same package, unbuilt
							clazz.parent
						)
					)
				}
				fileSpecBuilder.addImport("kotlinx.cinterop", "reinterpret")
				addSuperclassConstructorParameter("%L", "$pointerName.reinterpret()")
			}

			clazz.implements.forEach { implements ->
				implementInterface(fileSpecBuilder, pointerName, implements, kotlinPackageName)
			}

			// Import methods
			clazz.methods.forEach {
				fileSpecBuilder.addImport(cPackageName, it.cIdentifier!!)
			}

			generateVarMethods(
				clazz.properties,
				kotlinPackageName,
				pointerName,
				clazz.methods
			) { p, n ->
				fileSpecBuilder.addImport(p, n)
			}
			generateMethods(kotlinPackageName, pointerName, clazz.methods) { p, n ->
				fileSpecBuilder.addImport(p, n)
			}

			val companionObj = TypeSpec.companionObjectBuilder()
				.apply {
					clazz.functions.forEach {
						val builder = FunctionBuilder(kotlinPackageName, it)
						builder.build(this)
					}
					addFunction(
						FunSpec.builder("wrap")
							.receiver(
								cpointerFor(
									pointerTypeName
								).copy(nullable = true)
							)
							.returns(ClassName("", clazz.name).copy(nullable = true))
							.addStatement("return·this?.wrap()").build()
					)
					addFunction(
						FunSpec.builder("wrap")
							.receiver(cpointerFor(pointerTypeName))
							.returns(ClassName("", clazz.name))
							.addStatement("return ${clazz.name}(this)")
							.build()
					)
				}

			clazz.signals.forEach { signal ->
				SignalBuilder(
					kotlinPackageName,
					fileSpecBuilder,
					clazz.name,
					pointerType,
					this,
					companionObj,
					signal
				).build()
			}

			addType(
				companionObj.build()
			)
		}.build()
		fileSpecBuilder.addType(typeSpecBuilder)
		fileSpecBuilder.build().writeTo(repoDir)
	}
}