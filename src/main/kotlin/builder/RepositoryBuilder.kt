package builder

import model.GIRRepository
import java.io.File

/**
 * 09 / 11 / 2022
 *
 * Builds each Repository
 */
class RepositoryBuilder(
	private val repo: GIRRepository,
	private val repoDir: File
) {

	fun build() {

		val pack = repo.pack.firstOrNull()
		if (pack == null) {
			logging.errorOut("Failed to build, no packages to work with")
			return
		}
		val packageName = resolveCPackageName(pack.name)
		val kotlinPackageName = resolveKotlinPackageName(pack.name)
		/**
		 * TODO Package renaming
		 */

		repo.namespace.forEach { nameSpace ->
			// IGNORE Constants
			// IGNORE Alias for now TODO alias in the future
			nameSpace.bitfields.forEach {
				CTypeMemory.add(
					CTypeMemory.ClassDescriptor(
						it.name,
						CTypeMemory.ClassType.DIRECT,
						kotlinPackageName,
						it.cType
					)
				)
			}
			nameSpace.enums.forEach { enumeration ->
				val builder =
					EnumBuilder(repoDir, packageName, kotlinPackageName, nameSpace, enumeration)
				builder.build()
			}

			nameSpace.interfaces.forEach { interfaze ->
				val builder =
					InterfaceBuilder(repoDir, packageName, kotlinPackageName, nameSpace, interfaze)
				builder.build()
			}

			nameSpace.classes.forEach { clazz ->
				val builder =
					ClassBuilder(repoDir, packageName, kotlinPackageName, nameSpace, clazz)
				builder.build()
			}
		}
	}
}