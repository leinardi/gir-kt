package logging

/**
 * 13 / 09 / 2022
 */
const val CRESET: String = "\u001B[0m"
const val CCYAN: String = "\u001B[36m"
const val CPURPLE: String = "\u001B[35m"
const val CRED: String = "\u001B[31m"
const val CGREEN: String = "\u001B[32m"
const val CYELLOW: String = "\u001B[15m"

fun warning(message: Any?) {
	println(CYELLOW + "Warning:\t" + message + CRESET)
}

fun errorOut(message: Any?) {
	println(CRED + "Error:\t" + message + CRESET)
}

fun info(message: Any?) {
	println(CGREEN + "Info:\t" + message + CRESET)
}
